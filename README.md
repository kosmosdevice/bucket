# Bucket

This is a simulation of the naive playing card game bucket. It intends to find
out if all bucket games are guaranteed to end in a finite number of turns. <br>
Author: Karl Walter <br>
Mean = 152.95 rounds, standard deviation = 91.45 rounds

## Plot of results
<img src="results_ordered.png" width="300"/> <br>
<img src="results.png" width="300"/>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <omp.h>


typedef struct card_t card_t;
typedef struct node_t node_t;


struct card_t {
    int val;
    int suit;
};

struct node_t {
    card_t* card;
    node_t* next;
};



void shuffle(int *array, size_t n) {
    for (size_t i = 0; i < n - 1; i++) {
      size_t j = i + random() / (RAND_MAX / (n - i) + 1);
      int t = array[j];
      array[j] = array[i];
      array[i] = t;
    }
}


void print_card(card_t* card) {
    char* suits[4];
    suits[0] = "hearts";
    suits[1] = "spades";
    suits[2] = "clubs";
    suits[3] = "diamonds";
    printf("%d of %s\n", card->val, suits[card->suit]);
}


int play_game(node_t* head_one, node_t* head_two) {
    node_t* head_deck = NULL;
   
    bool player_one = true;
    int top = 0;
    int rounds = 0 ;
    while (head_one != NULL && head_two != NULL) {
        if (head_deck != NULL) {
            top = head_deck->card->val;
            //printf("Card on top: ");
            //print_card(head_deck->card);
        }
        else {
            //printf("Empty board!\n");
        }
        if (player_one) {
            //printf("Player ones plays: ");
            //print_card(head_one->card);
            if (head_one->card->val >= top) {
                node_t* new_head_one = head_one->next;
                node_t* old_head_deck = head_deck;
                head_deck = head_one;
                head_deck->next = old_head_deck;
                head_one = new_head_one;
            }
            else {
                node_t* current = head_one;
                while (current->next != NULL) {
                    current = current->next;
                }
                current->next = head_deck;
                head_deck = NULL;
                top = 0;
            }
            player_one = false;
        }
        else {
            //printf("Player two plays: ");
            //print_card(head_two->card);
            if (head_two->card->val >= top) { 
                node_t* new_head_two = head_two->next;
                node_t* old_head_deck = head_deck;
                head_deck = head_two;
                head_deck->next = old_head_deck;
                head_two = new_head_two;
            }
            else {
                node_t* current = head_two;
                while (current->next != NULL) {
                    current = current->next;
                }
                current->next = head_deck;
                head_deck = NULL;
                top = 0;
            }
            player_one = true;
        }
        rounds++;
    }
    if(player_one)
        printf("1\n");
    return rounds;
}


void one_game() {
    /* 
     * Create and shuffle deck of cards
     */
    card_t deck[52];
    int indices[52];
    for (int i = 0; i < 52; i++) {
        indices[i] = i;
    }
    shuffle(indices, 52);
    
    for (int i = 0; i < 52; i++) {
        int index = indices[i];
        deck[index].val = i % 13 + 2;
        deck[index].suit = i / 13;
    }
    
    /* 
     * Initiate two linked lists, one
     * for each player
     */
    node_t* head_one = (node_t*) malloc(sizeof(node_t));
    node_t* head_two = (node_t*) malloc(sizeof(node_t));
    /*
     * Insert every other card in every other
     * players deck
     */
    for (int i = 0; i < 52; i++) {
        card_t* card = &deck[i];
        if (i == 0) {
            head_one->card = card;
            continue;
        } 
        else if (i == 1) {
            head_two->card = card;
            continue;
        }
        node_t* current;
        if (i % 2 == 0) {
            current = head_one;
        } 
        else {
            current = head_two;
        }
        node_t* last = current;
        while (current != NULL) {
            last = current;
            current = current->next;
        }
        last->next = (node_t*) malloc(sizeof(node_t));
        last->next->card = card;
    }
    int rounds = play_game(head_one, head_two);
    //printf("Game took %d rounds\n", rounds);
    //if (head_one == NULL) {printf("Player One Won\n");} else {printf("Player Two Won\n");}
    //printf("%d\n", rounds);
}


int main(){
    int num_games = 1000000;
    //int result[num_games];
    //int sum = 0;
    srandom(42);
    omp_set_num_threads(omp_get_max_threads());
    #pragma omp parallel for
    for (int i = 0; i < num_games; i++) {
        //result[i] = one_game();
        //sum += result[i];
        one_game();
    }
    //int mean = sum / num_games;
    //printf("%d\n", mean);
    return 1;
}
